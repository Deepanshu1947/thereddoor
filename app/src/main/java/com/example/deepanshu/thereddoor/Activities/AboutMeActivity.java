package com.example.deepanshu.thereddoor.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.deepanshu.thereddoor.Entities.ApiUserResponse;
import com.example.deepanshu.thereddoor.R;
import com.example.deepanshu.thereddoor.Retrofit.RestClient;
import com.example.deepanshu.thereddoor.Utilities.Validations;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AboutMeActivity extends BaseActivity {
    EditText etAboutMe;
    TextView tvCount;
    Button btSave;
    TextWatcher twNumChar;
    int charCount = 0;
    Validations validate;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_me);
        init();
        setOnClick();
    }

    public void init() {
        tvCount = (TextView) findViewById(R.id.num_characters);
        btSave = (Button) findViewById(R.id.button_save);
        etAboutMe = (EditText) findViewById(R.id.about_me_text_area);
        validate = new Validations();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        setActivityHeader(R.string.title_activity_about_me);
        setIvBackOnClick();
        twNumChar = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                charCount = s.length();
            }

            @Override
            public void afterTextChanged(Editable s) {
                tvCount.setText(charCount + "|" + ABOUT_ME_MAX_LENGTH);
            }
        };

    }

    public void setOnClick() {
        etAboutMe.addTextChangedListener(twNumChar);
        btSave.setOnClickListener(AboutMeActivity.this);
    }

    @Override
    public void onClick(View v) {
        hideKeybord();
        switch (v.getId()) {
            case R.id.button_save:
                validate();
                break;
        }
    }

    public void validate() {
        String aboutData = etAboutMe.getText().toString();
        Boolean validData = validate.aboutMeValidator(aboutData);
        String token = sharedPreferences.getString(ACCESS_TOKEN, "");
        if (validData) {
            hideKeybord();
            showProgressDialog(PROCESSING);
            new RestClient().getService().putAboutMe(BEARER + token,
                    aboutData.toString(), new Callback<ApiUserResponse>() {
                        @Override
                        public void success(ApiUserResponse apiUserResponse, Response response) {
                            startActivity(new Intent(AboutMeActivity.this, AddAddressActivity.class));
                            finish();
                            hideProgressDialog();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            hideProgressDialog();
                            showToast(error.getMessage());
                        }
                    });
        } else
            showToast(INVALID_ABOUT_ME);
    }
}

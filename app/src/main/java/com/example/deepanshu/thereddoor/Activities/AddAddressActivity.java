package com.example.deepanshu.thereddoor.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.example.deepanshu.thereddoor.Entities.ApiUserResponse;
import com.example.deepanshu.thereddoor.GoogleMapsApi.GeoCodingLocation;
import com.example.deepanshu.thereddoor.R;
import com.example.deepanshu.thereddoor.Retrofit.RestClient;
import com.example.deepanshu.thereddoor.Utilities.Validations;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AddAddressActivity extends BaseActivity {
    EditText etZip, etCity, etStreet, etState, etSuite;
    Button btSave;
    RelativeLayout rlSearchMap;
    Validations validate;
    Intent intent;
    String zip, city, street, state, suite;
    double lat, lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        init();
        setOnClick();
    }

    public void init() {
        btSave = (Button) findViewById(R.id.button_save);
        etZip = (EditText) findViewById(R.id.et_zip);
        etCity = (EditText) findViewById(R.id.et_city);
        etState = (EditText) findViewById(R.id.et_state);
        etStreet = (EditText) findViewById(R.id.et_street);
        etSuite = (EditText) findViewById(R.id.et_apartment);
        rlSearchMap = (RelativeLayout) findViewById(R.id.rl_search_map);
        validate = new Validations();
    }

    public void setOnClick() {
        setActivityHeader(R.string.title_activity_address);
        setIvBackOnClick();
        btSave.setOnClickListener(AddAddressActivity.this);

        rlSearchMap.setOnClickListener(AddAddressActivity.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_save:
                validate();
                break;
            case R.id.rl_search_map:
                intent = new Intent(AddAddressActivity.this, GoogleMapLocationCallActivity.class);
                startActivityForResult(intent, ADDRESS_REQUEST_CODE);
                break;
        }
    }

    private int isGooglePlayServicesAvailable() {
        //checking the google services(Internet Connection)
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        return status;

    }

    public void validate() {

        zip = etZip.getText().toString();
        city = etCity.getText().toString();
        street = etStreet.getText().toString();
        state = etState.getText().toString();
        suite = etSuite.getText().toString();
        if (zip != null && !zip.isEmpty()
                && city != null && !city.isEmpty()
                && street != null && !street.isEmpty()
                && state != null && !state.isEmpty()
                && suite != null && !suite.isEmpty()) {
            if (lat == 0.0 || lng == 0.0) {
                String agentAddress = suite + " " + street + " " + city + " " + state;
                int status = isGooglePlayServicesAvailable();
                if (status != ConnectionResult.SUCCESS) {
                    showToast(MSG_NO_INTERNET_CONNECTION);
                } else {
                    GeoCodingLocation convertLocationToLatLng = new GeoCodingLocation();
                    convertLocationToLatLng.convertToAddressDetails(agentAddress, getApplicationContext(), new LatLngHandler());
                }
            } else {
                callRest();
            }
        } else
            showToast(NULL_VALUE);
    }

    public void callRest() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(AddAddressActivity.this);
        String token = sharedPreferences.getString(ACCESS_TOKEN, "");
        hideKeybord();
        showProgressDialog(PROCESSING);
        new RestClient().getService().postAddress(ANONYMOUS + token, street, suite, city, state, zip, lat, lng
                , new Callback<ApiUserResponse>() {
            @Override
            public void success(ApiUserResponse apiUserResponse, Response response) {
                startActivity(new Intent(AddAddressActivity.this, SetAvailableActivity.class));
                finish();
                hideProgressDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                hideProgressDialog();
                showToast(error.getMessage());
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == ADDRESS_REQUEST_CODE) && (resultCode == RESULT_OK)) ;
        String agentAddress = data.getStringExtra("address");
        int status = isGooglePlayServicesAvailable();
        if (agentAddress != null) {
            if (status == ConnectionResult.SUCCESS) {
                GeoCodingLocation convertLocationToLatLng = new GeoCodingLocation();
                convertLocationToLatLng.convertToAddressDetails(agentAddress, getApplicationContext(), new AddressHandler());
            } else
                showToast(MSG_NO_INTERNET_CONNECTION);
        } else
            showToast(NO_ADDRESS_RECIEVED);
    }

    //to find full address
    public class AddressHandler extends android.os.Handler {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            switch (msg.what) {
                case ADDRESS_FOUND_MESSAGE:
                    etCity.setText(bundle.getString(CITY));
                    etZip.setText(bundle.getString(ZIP));
                    etState.setText(bundle.getString(STATE));
                    etStreet.setText(bundle.getString(STREET));
                    etSuite.setText(bundle.getString(SUITE));
                    double[] latLong;
                    latLong = bundle.getDoubleArray(LAT_LONG_BUNDLE);
                    lat = latLong[0];
                    lng = latLong[1];
                    break;
                case ADDRESS_NOT_FOUND_MESSAGE:
                default:
                    showToast(MSG_ADDRESS_NOT_FOUND);
            }
        }
    }

    // to find only LatLong
    private class LatLngHandler extends android.os.Handler {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            double[] latLong = bundle.getDoubleArray(LAT_LONG_BUNDLE);

            switch (msg.what) {
                case ADDRESS_FOUND_MESSAGE:
                    lat = latLong[0];
                    lng = latLong[1];
                    callRest();
                    break;
                case ADDRESS_NOT_FOUND_MESSAGE:
                default:
                    showToast(MSG_ADDRESS_NOT_FOUND);
            }
        }
    }
}

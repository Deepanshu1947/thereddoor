package com.example.deepanshu.thereddoor.Activities;
//androidpolice.com


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.deepanshu.thereddoor.R;
import com.example.deepanshu.thereddoor.Utilities.Conventions;

/**
 * Created by Deepanshu on 11/7/2015.
 */
public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener, Conventions {
    Toast toast;
    ProgressDialog proDialog;
    TextView tvActivityStatus;
    ImageButton ivBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toast = new Toast(this);
        proDialog = new ProgressDialog(this);
    }

    public abstract void init();

    public abstract void setOnClick();

    public void setActivityHeader(int activityHeader) {
        tvActivityStatus = (TextView) findViewById(R.id.status_text);
        tvActivityStatus.setText(activityHeader);
        ivBack = (ImageButton) findViewById(R.id.button_back);

    }

    public void setIvBackOnClick() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    public void showProgressDialog(String header, String msg) {
        if (!proDialog.isShowing()) {
            proDialog.setTitle(header);
            proDialog.setMessage(msg);
            proDialog.setCancelable(false);
            proDialog.show();
        }
    }


    public void showProgressDialog(String msg) {
        if (!proDialog.isShowing()) {
            proDialog.setMessage(msg);
            proDialog.setCancelable(false);
            proDialog.show();
        }
    }

    public void hideProgressDialog() {
        if (proDialog.isShowing())
            proDialog.dismiss();
    }

    public void showToast(String msg) {
        toast.cancel();
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public void hideKeybord() {
//to hide keyboard if shown
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive())
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    public void showKeybord() {
//to show keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInputFromInputMethod(getCurrentFocus().getWindowToken(), 0);
    }
}

package com.example.deepanshu.thereddoor.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.deepanshu.thereddoor.Entities.ApiUserResponse;
import com.example.deepanshu.thereddoor.Fragments.AppointmentsFragment;
import com.example.deepanshu.thereddoor.R;
import com.example.deepanshu.thereddoor.Retrofit.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class FragmentsMainActivity extends BaseActivity {
    ImageView navProfilePic;
    ActionBarDrawerToggle mDrawerToggle;
    ArrayAdapter drawerAdapter;
    LinearLayout drawerLayout;
    ListView drawerList;
    TextView navUserName, navTerms;
    android.support.v4.widget.DrawerLayout mDrawerLayout;
    android.support.v4.app.FragmentManager fragmentManager;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragments_activity_main);
        init();
        setOnClick();
    }

    @Override
    public void init() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        fragmentManager = getSupportFragmentManager();
        fragmentReplace(new AppointmentsFragment(), APPOINTMENTS_FRAGMENT_TAG);
    }

    @Override
    public void setOnClick() {
        setActivityHeader(R.string.title_activity_appointments);
        ivBack.setImageResource(R.drawable.menu_bar_selector);
        ivBack.setOnClickListener(FragmentsMainActivity.this);
        setNavigationDrawer();
    }


    public void setNavigationDrawer() {

        //initializing the layout variables
        mDrawerLayout = (android.support.v4.widget.DrawerLayout) findViewById(R.id.drawer_layout_base);
        drawerLayout = (LinearLayout) findViewById(R.id.ll_navigation_drawer);
        drawerList = (ListView) findViewById(R.id.drawer_list);
        navUserName = (TextView) findViewById(R.id.user_name);
        navTerms = (TextView) findViewById(R.id.nav_terms);
        navProfilePic = (ImageView) findViewById(R.id.user_profile_pic);

        //Setting the name and pic
        navTerms.setOnClickListener(this);
        navUserName.setText(sharedPreferences.getString(USER_FIRST_NAME, ANONYMOUS));
        navProfilePic.setImageResource(R.mipmap.small_placeholder);

        //Setting the conent of nav drawer list view
        drawerAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, NAV_DRAWER_ARRAY_LIST);
        drawerList.setAdapter(drawerAdapter);

        //to perform actions on navigation drawer list menu
        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mDrawerLayout.closeDrawer(drawerLayout);
                switch (position) {
                    case 0://HOME
                        //fragmentReplace();
                        break;
                    case 1://MY ACCOUNT
                        //fragmentReplace();
                        break;
                    case 2://OPEN APPOINTMENTS
                        fragmentReplace(new AppointmentsFragment(), APPOINTMENTS_FRAGMENT_TAG);
                        break;
                    case 3://ABOUT ME
                        //fragmentReplace();
                        break;
                    case 4://BOOKING HISTORY
                        //fragmentReplace();
                        break;
                    case 5://RAISE ALARM
                        //fragmentReplace();
                        break;
                    case 6://logout
                        logoutCall();
                        break;
                }
            }
        });

        // to open and close navigation drawer on swipe or click
        mDrawerToggle = new ActionBarDrawerToggle(this,
                mDrawerLayout,
                R.string.drawer_open,
                R.string.drawer_close) {

            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_back:
                mDrawerLayout.openDrawer(drawerLayout);
                break;
            case R.id.nav_terms:
                startActivity(new Intent(this, TermsConditionsActivity.class));
                break;
        }
    }

    public void fragmentReplace(android.support.v4.app.Fragment fragment, String tag) {
        //to replace the currrent fragment with the provided one
        if (fragmentManager != null) {
            fragmentManager.beginTransaction()
                    .replace(R.id.relative_fragment, fragment, tag)
                    .commit();
        }
    }

    void logoutCall() {
        //token generated when signed in/up
        String token = sharedPreferences.getString(ACCESS_TOKEN, "");
        hideKeybord();
        showProgressDialog(PROCESSING);
        //Api hit for logout
        new RestClient().getService().putLogout("Bearer " + token
                , new Callback<ApiUserResponse>() {
            @Override
            public void success(ApiUserResponse loginRegisterResponse, Response response) {
                hideProgressDialog();
                startActivity(new Intent(FragmentsMainActivity.this, SignInActivity.class));
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
                hideProgressDialog();
                showToast(error.getMessage());
            }
        });
    }
}

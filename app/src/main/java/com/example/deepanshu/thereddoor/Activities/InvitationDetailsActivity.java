package com.example.deepanshu.thereddoor.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.deepanshu.thereddoor.Entities.ParentRow;
import com.example.deepanshu.thereddoor.R;

public class InvitationDetailsActivity extends BaseActivity {
    ParentRow appointmentData;
    ImageView ivProfilePic,ivLocation;
    TextView tvDateandTime, tvTimeLeft, tvName, tvService, tvDuration, tvLocationStatus, tvLocation, tvCustomerNotes;
    Button btAccept, btDecline;
    Intent currIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitation_details);
        init();
        setOnClick();
    }

    public void init() {


        ivProfilePic = (ImageView) findViewById(R.id.image_view_profile_pic);
        ivLocation = (ImageView) findViewById(R.id.button_location);
        tvDateandTime = (TextView) findViewById(R.id.tv_time_date);
        tvTimeLeft = (TextView) findViewById(R.id.tv_time_left);
        tvName = (TextView) findViewById(R.id.profile_name);
        tvService = (TextView) findViewById(R.id.tv_service);
        tvDuration = (TextView) findViewById(R.id.tv_duration);
        tvLocation = (TextView) findViewById(R.id.tv_location);
        tvLocationStatus = (TextView) findViewById(R.id.tv_location_status);
        tvCustomerNotes = (TextView) findViewById(R.id.customer_notes);
        btAccept = (Button) findViewById(R.id.button_accept);
        btDecline = (Button) findViewById(R.id.button_decline);
        currIntent = getIntent();
        appointmentData = (ParentRow) currIntent.getSerializableExtra(APPOINTMENTS_DATA);
    }

    public void setOnClick() {
        setActivityHeader(R.string.title_activity_invitation_details);
        setIvBackOnClick();
        ivLocation.setVisibility(View.GONE);
        tvLocationStatus.setText(R.string.service_at);
        btDecline.setOnClickListener(InvitationDetailsActivity.this);
        btAccept.setOnClickListener(InvitationDetailsActivity.this);
        tvDateandTime.setText(appointmentData.getAppointmentDate().toString());
        ivProfilePic.setImageResource(appointmentData.getPic());
        tvTimeLeft.setText(appointmentData.getItems().getInvitationDate().toString());
        tvName.setText(appointmentData.getName().toString());
        tvService.setText(appointmentData.getItems().getService().toString());
        tvDuration.setText(appointmentData.getItems().getDuration().toString());
        tvLocation.setText(appointmentData.getItems().getLocation().toString());
        tvCustomerNotes.setText(appointmentData.getItems().getCustomerNotes().toString());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_decline:
                super.onBackPressed();
                break;
            case R.id.button_accept:
                super.onBackPressed();
                break;
        }
    }
}

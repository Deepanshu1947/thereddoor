package com.example.deepanshu.thereddoor.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.deepanshu.thereddoor.R;

public class SetAvailableActivity extends BaseActivity {
    Button btNext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_available);
        init();
        setOnClick();
    }

    public void init() {
        btNext = (Button) findViewById(R.id.button_next);

    }

    public void setOnClick() {
       setActivityHeader(R.string.title_activity_set_available);
        setIvBackOnClick();
        btNext.setOnClickListener(SetAvailableActivity.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_next:
                 startActivity(new Intent(SetAvailableActivity.this,FragmentsMainActivity.class));
                finish();
                break;
        }
    }
}

package com.example.deepanshu.thereddoor.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.deepanshu.thereddoor.Entities.ApiUserResponse;
import com.example.deepanshu.thereddoor.Entities.RegionResponse;
import com.example.deepanshu.thereddoor.R;
import com.example.deepanshu.thereddoor.Retrofit.RestClient;
import com.example.deepanshu.thereddoor.Utilities.Validations;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedString;

public class SignUpActivity extends BaseActivity {
    EditText etFirstName, etLastName, etPassword, etConfirmPassword, etRegEmpId, etEmail, etMobileNumber;
    Spinner spGender, spRegion;
    TextView tvSignIn, tvTerms;
    Button btSignUp;
    ArrayList<RegionResponse.Region> regionList = new ArrayList<>();
    ArrayAdapter<RegionResponse.Region> regionSpinnerAdap;
    Validations validate;
    String fName, lName, emailId, mobNo, password, confirmPassword, empId, gender, region;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        init();
        setOnClick();
        gettingTheData();

    }

    public void init() {
        etFirstName = (EditText) findViewById(R.id.et_first_name);
        etLastName = (EditText) findViewById(R.id.et_last_name);
        etPassword = (EditText) findViewById(R.id.et_password);
        etConfirmPassword = (EditText) findViewById(R.id.et_confirm_password);
        etRegEmpId = (EditText) findViewById(R.id.et_registered_emp_id);
        etEmail = (EditText) findViewById(R.id.et_email);
        spGender = (Spinner) findViewById(R.id.sp_gender);
        etMobileNumber = (EditText) findViewById(R.id.et_mobile_number);
        spRegion = (Spinner) findViewById(R.id.sp_region);
        tvSignIn = (TextView) findViewById(R.id.tv_sign_in);
        tvTerms = (TextView) findViewById(R.id.tv_terms);
        btSignUp = (Button) findViewById(R.id.button_sign_up);
        validate = new Validations();
        RegionResponse.Region regHint = new RegionResponse.Region();
        regHint.setRegionName("Region");
        regionList.add(regHint);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        regionSpinnerAdap = new ArrayAdapter<RegionResponse.Region>(this, android.R.layout.simple_list_item_1, regionList) {
            @Override
            public RegionResponse.Region getItem(int position) {
                return super.getItem(position);
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater = (LayoutInflater) SignUpActivity.this.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                View view;
                if (position == 0) {
                    view = inflater.inflate(R.layout.hidden_layout, parent, false); // Hide first row
                } else {
                    view = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
                    TextView textView = (TextView) view.findViewById(android.R.id.text1);
                    textView.setText(regionList.get(position).getRegionName().toString());
                    textView.setTextColor(Color.BLACK);
                }
                return view;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                convertView = View.inflate(SignUpActivity.this, android.R.layout.simple_list_item_1, null);
                TextView textView = (TextView) convertView.findViewById(android.R.id.text1);
                textView.setText(regionList.get(position).getRegionName().toString());
                return convertView;
            }
        };
        spGender.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.gender_list)) {
            String[] arrayList = getResources().getStringArray(R.array.gender_list);

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater = (LayoutInflater) SignUpActivity.this.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

                View view;
                if (position == 0) {
                    view = inflater.inflate(R.layout.hidden_layout, parent, false); // Hide first row
                } else {
                    view = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
                    TextView textView = (TextView) view.findViewById(android.R.id.text1);
                    textView.setText(arrayList[position].toString());
                    textView.setTextColor(Color.BLACK);
                }
                return view;
            }
        });

    }

    public void setOnClick() {
        setActivityHeader(R.string.title_activity_sign_up);
        ivBack.setOnClickListener(this);
        tvSignIn.setText(Html.fromHtml(getString(R.string.already_member)));
        tvTerms.setText(Html.fromHtml(getString(R.string.agree_terms)));
        tvSignIn.setOnClickListener(SignUpActivity.this);
        tvTerms.setOnClickListener(SignUpActivity.this);
        btSignUp.setOnClickListener(SignUpActivity.this);
    }

    public void gettingTheData() {
        new RestClient().getService().getRegions(new Callback<RegionResponse>() {
            @Override
            public void success(RegionResponse regionResponse, Response response) {
                regionList.addAll(regionResponse.getData());
                regionSpinnerAdap.notifyDataSetChanged();
                spRegion.setSelection(0);
            }

            @Override
            public void failure(RetrofitError error) {
                showToast(error.getMessage());
            }
        });
        spRegion.setAdapter(regionSpinnerAdap);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_terms:
                startActivity(new Intent(SignUpActivity.this, TermsConditionsActivity.class));
                break;
            case R.id.button_sign_up:
                validate();
                break;
            case R.id.tv_sign_in:
            case R.id.button_back:
                startActivity(new Intent(SignUpActivity.this, SignInActivity.class));
                finish();
                break;
        }
    }

    public void validate() {

        fName = etFirstName.getText().toString();
        lName = etLastName.getText().toString();
        emailId = etEmail.getText().toString();
        mobNo = etMobileNumber.getText().toString();
        password = etPassword.getText().toString();
        confirmPassword = etConfirmPassword.getText().toString();
        empId = etRegEmpId.getText().toString();
        gender = spGender.getSelectedItem().toString();
        region = ((RegionResponse.Region) spRegion.getSelectedItem()).getRegionName();

        Boolean validFirstName = validate.nameValidator(fName);
        Boolean validLastName = validate.nameValidator(lName);
        Boolean validEmail = validate.emailValidator(emailId);
        Boolean validPhone = validate.phoneValidator(mobNo);
        Boolean validPassword = validate.passwordValidator(password);
        Boolean validEmpId = validate.empIdValidator(empId);
        Boolean validRegion = validate.genderValidator(gender, getResources().getStringArray(R.array.gender_list));
        Boolean validGender = validate.RegionValidator(region, regionList);
        Boolean validConfirmPassword = validate.confirmPasswordValidator(password, confirmPassword);

        if (validFirstName && validLastName && validEmail && validPassword && validConfirmPassword && validEmpId && validRegion && validGender) {
            retrofitCall();
        } else if (!validFirstName)
            showToast(INVALID_FIRST_NAME);
        else if (!validLastName)
            showToast(INVALID_LAST_NAME);
        else if (!validEmail)
            showToast(INVALID_EMAIL);
        else if (!validPassword)
            showToast(INVALID_PASSWORD);
        else if (!validConfirmPassword)
            showToast(INVALID_CONFIRM_PASSWORD);
        else if (!validPhone)
            showToast(INVALID_PHONE);
        else if (!validEmpId)
            showToast(INVALID_EMP_ID);
        else if (!validRegion)
            showToast(INVALID_REGION);
        else if (!validGender)
            showToast(INVALID_GENDER);

    }

    public void retrofitCall() {

        hideKeybord();
        showProgressDialog(PROCESSING);
        new RestClient().getService().getRegister(new TypedString(fName),
                new TypedString(lName),
                new TypedString(emailId), new TypedString(empId), new TypedString(region),
                new TypedString(password), new TypedString(mobNo), new TypedString(DEVICE_TYPE),
                new TypedString(sharedPreferences.getString(DEVICE_TOKEN, "")),
                new TypedString(gender), new Callback<ApiUserResponse>() {
                    @Override
                    public void success(ApiUserResponse registerResponse, Response response) {
                        sharedPreferences.edit().putString(ACCESS_TOKEN, registerResponse.getData().getToken());
                        showToast(SUCCESS);
                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(ACCESS_TOKEN, registerResponse.getData().getToken().toString());
                        editor.putString(USER_FIRST_NAME, registerResponse.getData().getAgent().getName().getFirstName().toString());
                        editor.putString(USER_LAST_NAME, registerResponse.getData().getAgent().getName().getLastName().toString());
                        editor.commit();
                        startActivity(new Intent(SignUpActivity.this, AboutMeActivity.class));
                        finish();
                        hideProgressDialog();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        hideProgressDialog();
                        showToast(error.getMessage());
                    }
                });
    }
}

package com.example.deepanshu.thereddoor.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.deepanshu.thereddoor.Fragments.AppointmentsFragment;
import com.example.deepanshu.thereddoor.Activities.ShowLocationOnMapActivity;
import com.example.deepanshu.thereddoor.Entities.ChildRow;
import com.example.deepanshu.thereddoor.Entities.ParentRow;
import com.example.deepanshu.thereddoor.R;

import java.util.ArrayList;

/**
 * Created by Deepanshu on 10/26/2015.
 */
public class ExpendableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<ParentRow> groups;

    public ExpendableListAdapter(Context context, ArrayList<ParentRow> groups) {
        this.context = context;
        this.groups = groups;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ChildRow data = groups.get(groupPosition).getItems();
        return data;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final ChildRow child = (ChildRow) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.view_item_expanded, null);
        }
        TextView tvService = (TextView) convertView.findViewById(R.id.tv_service);
        TextView tvDuration = (TextView) convertView.findViewById(R.id.tv_duration);
        TextView tvLocation = (TextView) convertView.findViewById(R.id.tv_location);
        ImageView ivlocation = (ImageView) convertView.findViewById(R.id.button_location);
        tvService.setText(child.getService().toString());
        tvDuration.setText(child.getDuration().toString());
        tvLocation.setText(child.getLocation().toString());
        ivlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent showLocationPage =new Intent(context, ShowLocationOnMapActivity.class);
                showLocationPage.putExtra("location", child.getLocation());
                context.startActivity(showLocationPage);

            }
        });
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ParentRow group = (ParentRow) getGroup(groupPosition);
        LayoutInflater inf = LayoutInflater.from(context);
        convertView = inf.inflate(R.layout.view_item, parent, false);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.image_view_profile_pic);
        TextView tvName = (TextView) convertView.findViewById(R.id.profile_name);
        TextView tvDate = (TextView) convertView.findViewById(R.id.tv_time_date);
        TextView tvTimeLeft = (TextView) convertView.findViewById(R.id.tv_time_left);
        tvTimeLeft.setText(group.getItems().getInvitationDate().toString());
        if (AppointmentsFragment.boolBtInvitation) {
            tvTimeLeft.setVisibility(View.VISIBLE);

        }else
            tvTimeLeft.setVisibility(View.GONE);

        tvName.setText(group.getName().toString());
        tvDate.setText(group.getAppointmentDate().toString());
        imageView.setImageResource(Integer.valueOf(group.getPic()));
        ExpandableListView eLV = (ExpandableListView) parent;
        eLV.expandGroup(groupPosition);
        return convertView;
    }


}

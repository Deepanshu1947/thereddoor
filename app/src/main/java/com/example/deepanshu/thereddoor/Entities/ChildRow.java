package com.example.deepanshu.thereddoor.Entities;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Deepanshu on 10/26/2015.
 */
public class ChildRow implements Serializable {
    private String service;
    private String duration;
    private String location;
    private String customerNotes;
    private String invitationDate;

    public String getInvitationDate() {
        return invitationDate;
    }

    public void setInvitationDate(String invitationDate) {
        this.invitationDate = invitationDate;
    }
    public String getCustomerNotes() {
        return customerNotes;
    }
    public void setCustomerNotes(String customerNotes) {
        this.customerNotes = customerNotes;
    }

    public ChildRow(String service, String duration, String customerNotes) {
        this.service = service;
        this.duration = duration;
        this.customerNotes = customerNotes;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}

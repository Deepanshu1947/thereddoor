package com.example.deepanshu.thereddoor.Entities;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Deepanshu on 10/26/2015.
 */
public class ParentRow implements Serializable {
    private int pic;
    private String Name;
    private ChildRow Items;
    private String appointmentDate;


    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public void setPic(int pic) {
        this.pic = pic;
    }

    public int getPic() {
        return pic;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public ChildRow getItems() {
        return Items;
    }

    public void setItems(ChildRow Items) {
        this.Items = Items;
    }

}


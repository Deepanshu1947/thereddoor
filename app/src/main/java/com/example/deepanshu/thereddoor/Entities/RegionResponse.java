package com.example.deepanshu.thereddoor.Entities;

import java.util.ArrayList;

/**
 * Created by Deepanshu on 11/2/2015.
 */
public class RegionResponse {
    String message;
    ArrayList<Region> data;

    public ArrayList<Region> getData() {
        return data;
    }

    public void setData(ArrayList<Region> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Created by Deepanshu on 11/3/2015.
     */
    public static class Region {
        String regionName;
        String _id;
        String regionNickname;
        String zip;
        int __v;

        public int get__v() {
            return __v;
        }

        public void set__v(int __v) {
            this.__v = __v;
        }

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getRegionNickname() {
            return regionNickname;
        }

        public void setRegionNickname(String regionNickname) {
            this.regionNickname = regionNickname;
        }

        public String getZip() {
            return zip;
        }

        public void setZip(String zip) {
            this.zip = zip;
        }

        public String getRegionName() {
            return regionName;
        }

        public void setRegionName(String regionName) {
            this.regionName = regionName;
        }
    }
}
package com.example.deepanshu.thereddoor.Entities;

import java.util.ArrayList;

/**
 * Created by Deepanshu on 11/3/2015.
 */
public class TermsResponse {
    String error;

    String message;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}

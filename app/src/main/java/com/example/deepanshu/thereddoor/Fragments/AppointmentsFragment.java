package com.example.deepanshu.thereddoor.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;

import com.example.deepanshu.thereddoor.Activities.InvitationDetailsActivity;
import com.example.deepanshu.thereddoor.Adapters.ExpendableListAdapter;
import com.example.deepanshu.thereddoor.Entities.ChildRow;
import com.example.deepanshu.thereddoor.Entities.ParentRow;
import com.example.deepanshu.thereddoor.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AppointmentsFragment extends BaseFragment {
    Button btScheduled, btInvitations;
    ExpendableListAdapter listAdapter;
    ArrayList<ParentRow> appointmentData = new ArrayList<>();
    ExpandableListView expList;
    Activity currActivity;
    View myView;
    //variable for displaying the invitation time left (a text view visibility through adapter)
    public static Boolean boolBtInvitation = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.activity_appointments, container, false);
        return myView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        setOnClick();
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    public void init() {

        currActivity = getActivity();

        //initializing the xml widgets
        btScheduled = (Button) myView.findViewById(R.id.button_scheduled);
        btInvitations = (Button) myView.findViewById(R.id.button_invitation);
        expList = (ExpandableListView) myView.findViewById(R.id.appointments_list);

        //selecting the butons from two
        setButoonSelected(btScheduled, btInvitations);

        //creating temporary data
        createList();

        listAdapter = new ExpendableListAdapter(currActivity, appointmentData);

    }

    public void setExpandableOnClick() {
        expList.setOnChildClickListener(
                new ExpandableListView.OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        Intent invitationDetailspage = new Intent(currActivity, InvitationDetailsActivity.class);
                        invitationDetailspage.putExtra("appointmentData", appointmentData.get(groupPosition));
                        startActivity(invitationDetailspage);
                        return true;
                    }
                });
        expList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                Intent invitationDetailspage = new Intent(currActivity, InvitationDetailsActivity.class);
                invitationDetailspage.putExtra(APPOINTMENTS_DATA, appointmentData.get(groupPosition));
                startActivity(invitationDetailspage);
                return false;
            }
        });
    }

    public void setExpandableNonClickable() {
        expList.setOnGroupClickListener(null);
        expList.setOnChildClickListener(null);
    }

    public void setOnClick() {
        //for setting the
        btScheduled.setOnClickListener(AppointmentsFragment.this);
        btInvitations.setOnClickListener(AppointmentsFragment.this);
        expList.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_scheduled:
                boolBtInvitation = false;
                setExpandableNonClickable();
                listAdapter.notifyDataSetChanged();
                setButoonSelected(btScheduled, btInvitations);
                break;
            case R.id.button_invitation:
                boolBtInvitation = true;
                listAdapter.notifyDataSetChanged();
                setButoonSelected(btInvitations, btScheduled);
                setExpandableOnClick();
                break;
        }
    }

    // for setting out the button background of selected button and second one non selected and its drawable
    public void setButoonSelected(Button btSelected, Button btNotSelected) {
        btSelected.setBackgroundResource(R.mipmap.big_btn_pressed);
        btSelected.setTextColor(Color.parseColor("#ffffff"));
        btNotSelected.setBackgroundResource(R.drawable.big_button_selector);
        btNotSelected.setTextColor(Color.parseColor("#474747"));
    }

    // for adding temporary data to the list (Must use api call to get the data)
    private void createList() {
        ParentRow appointment;
        for (int i = 0; i < 10; i++) {
            appointment = new ParentRow();
            appointment.setPic(R.mipmap.small_placeholder);
            appointment.setName("Testing :- " + i);
            SimpleDateFormat sdf = new SimpleDateFormat("h:mm a, MMMM dd,yyyy");
            appointment.setAppointmentDate(sdf.format(new Date()));
            ChildRow child = new ChildRow("Temp Service " + i, "Temp Duration " + i, "Empty Notes " + i);
            child.setInvitationDate("temp invite " + i);
            child.setLocation("Deen Dayal Upadhyay Marg, Mata Sundari Railway Colony, New Delhi, Delhi");
            appointment.setItems(child);
            appointmentData.add(appointment);
        }
    }
}

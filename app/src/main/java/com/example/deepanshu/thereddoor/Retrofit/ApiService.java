package com.example.deepanshu.thereddoor.Retrofit;

import com.example.deepanshu.thereddoor.Entities.RegionResponse;
import com.example.deepanshu.thereddoor.Entities.TermsResponse;
import com.example.deepanshu.thereddoor.Entities.ApiUserResponse;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.mime.TypedString;

/**
 * Created by Deepanshu on 11/2/2015.
 */
public interface ApiService {
    String GET_REGIONS = "/api/agent/region";
    String GET_TERMS = "/api/agent/termsAndPolicy";
    String POST_LOGIN = "/api/agent/login";
    String PUT_LOGOUT = "/api/agent/logout";
    String PUT_ABOUT_ME = "/api/agent/aboutme";
    String POST_REGISTER = "/api/agent/register";
    String POST_ADDRESS = "/api/agent/address";
    String GET_APPOINTMENTS = "/api/agent/yourAppointment";

    @GET(GET_REGIONS)
    void getRegions(Callback<RegionResponse> callback);

    @GET(GET_TERMS)
    void getTerms(Callback<TermsResponse> callback);

    @GET(GET_APPOINTMENTS)
    void getAppointments(Callback<RegionResponse> callback);

    @FormUrlEncoded
    @POST(POST_LOGIN)
    void postLogin(@Field("emailId") String email,
                   @Field("password") String password,
                   @Field("deviceType") String deviceType,
                   @Field("deviceToken") String deviceToken, Callback<ApiUserResponse> callback);

    @FormUrlEncoded
    @POST(POST_ADDRESS)
    void postAddress(@Header("authorization") String authorization,
            @Field("streetAddress") String streetAddress,
                     @Field("apartment") String apartment,
                     @Field("city") String city,
                     @Field("state") String state,
                     @Field("zip") String zip,
                     @Field("latitude") double latitude,
                     @Field("longitude") double longitude,
                     Callback<ApiUserResponse> callback);

    @PUT(PUT_LOGOUT)
    void putLogout(@Header("authorization") String authorization, Callback<ApiUserResponse> callback);

    @FormUrlEncoded
    @PUT(PUT_ABOUT_ME)
    void putAboutMe(@Header("authorization") String authorization, @Field("aboutMe") String aboutMe, Callback<ApiUserResponse> callback);

    @Multipart
    @POST(POST_REGISTER)
    void getRegister(@Part("firstName") TypedString firstName,
                     @Part("lastName") TypedString lastName,
                     @Part("emailId") TypedString emailId,
                     @Part("employeeId") TypedString employeeId,
                     @Part("region") TypedString region,
                     @Part("password") TypedString password,
                     @Part("phoneNumber") TypedString phoneNumber,
                     @Part("deviceType") TypedString deviceType,
                     @Part("deviceToken") TypedString deviceToken,
                     @Part("gender") TypedString gender,
                     Callback<ApiUserResponse> callback);

}

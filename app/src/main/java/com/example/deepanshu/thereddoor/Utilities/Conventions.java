package com.example.deepanshu.thereddoor.Utilities;

/**
 * Created by Deepanshu on 10/23/2015.
 */
public interface Conventions {

    //Constraints for all activities
    int SPLASH_SLEEP = 3,
            ADDRESS_FOUND_MESSAGE = 1,
            ADDRESS_NOT_FOUND_MESSAGE = 2,
            ADDRESS_REQUEST_CODE = 1;
    String LAT_LONG_BUNDLE = "latitude_longitude",
            APPOINTMENTS_DATA = "appointmentData";
    String[] NAV_DRAWER_ARRAY_LIST = new String[]{"HOME"
            , "MY ACCOUNT",
            "OPEN APPOINTMENTS"
            , "ABOUT ME",
            "BOOKING HISTORY",
            "RAISE ALARM",
            "LOGOUT"};
    String
            TERMS_AND_SERVICES="Terms and services",
            ANONYMOUS="Anonymous",
            BEARER = "Bearer ",
            SUCCESS = "Success",
            ALERT = "Alert",
            RETRY = "RETRY",
            EXIT = "Exit",
            YES = "Yes",
            NO = "No",
            PROCESSING = "Processing!!";

    //Constraints for Fragments Tags
    String APPOINTMENTS_FRAGMENT_TAG = "Appointment Fragment";

    //Constraints for Google map Activities
    String TAG = "GeocodingLocation",
            MSG_ADDRESS_NOT_FOUND = "Unable to find address",
            MSG_NO_LOCATION_PERMISSIONS = "GPS Services disabled....",
            MSG_NO_INTERNET_CONNECTION = "Check Your Internet Connection",
            CUST_LOC = "Customer Location",
            PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place",
            TYPE_AUTOCOMPLETE = "/autocomplete",
            OUT_JSON = "/json";

    //Server Key from Google Console for Searching Places
    String PLACES_KEY = "AIzaSyAnY_UDcXQ-DPAjNLDqzZ2aiXhSRujL6jg";

    //India's Latitude and longitude, to show if no Location Found
    double DEFAULT_LATITUDE = 28.619570,
            DEFAULT_LONGITUDE = 77.088104;


    //Validation Constraints

    String EMAIL_CONSTRAINT = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";//simple email regular expression
    String PHONE_CONSTRAINT = "^[1-9]-\\d{9}";//first number non zero and rest any number with length 10
    String NAME_CONSTRAINT = "^[a-zA-Z].{3,15}$";//upper case and lower case letters Length:- between 3 to 15
    String PASSWORD_CONSTRAINT = "((?=.*\\d)(?=.*[a-zA-Z]).{6,32})";//accepts numbers uppercase and lowercase alphabets and length:- between 6 to 32

    //Validation for AboutMeActivity text Provided by backend
    int ABOUT_ME_MIN_LENGTH = 20,
            ABOUT_ME_MAX_LENGTH = 140;

    //Toast Messages
    String NULL_VALUE = "All Fields Are Mandatory",
            INVALID_EMAIL = "Invalid Email Id",
            INVALID_PASSWORD = "Invalid Password",
            INVALID_CONFIRM_PASSWORD = "Confirm Password doesn't match",
            INVALID_PHONE = "Invalid Phone",
            INVALID_FIRST_NAME = "Invalid First Name",
            INVALID_LAST_NAME = "Invalid Last Name",
            INVALID_EMP_ID = "Invalid Employee Id",
            INVALID_ABOUT_ME = "Length should be between " + ABOUT_ME_MIN_LENGTH + " to " + ABOUT_ME_MAX_LENGTH + " characters",
            INVALID_GENDER = "Select Gender",
            INVALID_REGION = "Select Gender";

    //Shared Preferences
    String DEVICE_TOKEN = "device Token",
            DEVICE_TYPE = "ANDROID",
            ACCESS_TOKEN = "Access Token",
            PROJECT_NUMBER = "198112734044",
            ADDRESS = "Address",
            USER_FIRST_NAME = "User First Name",
            USER_LAST_NAME = "User Last Name",
            ZIP = "zip",
            CITY = "city",
            STREET = "street",
            STATE = "state",
            SUITE = "suite",
            NO_ADDRESS_RECIEVED = "No Address Recieved";
}

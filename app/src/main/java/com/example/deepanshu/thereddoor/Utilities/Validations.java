package com.example.deepanshu.thereddoor.Utilities;

import com.example.deepanshu.thereddoor.Entities.RegionResponse;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Deepanshu on 11/1/2015.
 */
public class Validations implements Conventions {

    public boolean emailValidator(String email) {
        Pattern pattern = Pattern.compile(EMAIL_CONSTRAINT);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public boolean phoneValidator(String phone) {
        Pattern pattern = Pattern.compile(PHONE_CONSTRAINT);
        Matcher matcher = pattern.matcher(phone);
        return matcher.matches();
    }  public boolean genderValidator(String phone,String[] genderArray) {
       if(phone.equalsIgnoreCase(genderArray[0].toString()))
       {return false;}
        else
           return true;
    }  public boolean RegionValidator(String region,ArrayList<RegionResponse.Region> regionsList) {
        if(region.equalsIgnoreCase(regionsList.get(0).getRegionName().toString()))
        {return false;}
        else
            return true; }

    public boolean passwordValidator(String password) {
        Pattern pattern = Pattern.compile(PASSWORD_CONSTRAINT);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public boolean confirmPasswordValidator(String pass, String confirmPass) {
        return pass.equals(confirmPass);
    }

    public boolean nameValidator(String name) {
        Pattern pattern = Pattern.compile(NAME_CONSTRAINT);
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    } public boolean empIdValidator(String empId) {
        if(true)
            return true;
        else
            return false;
    }
    public boolean aboutMeValidator(String aboutMe) {
        int count=aboutMe.length();
        if(count>ABOUT_ME_MIN_LENGTH&&count<ABOUT_ME_MAX_LENGTH)
        return true;
        else
            return false;
    }

}
